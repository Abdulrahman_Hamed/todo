import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  todos = [{ title: 'todo 1', id: '1', description: 'This is description for todo 1', dueDate: `${new Date()}`, isDone: false },
  { title: 'todo 2', id: '2', description: 'This is description for todo 2', dueDate: `${new Date()}`, isDone: true }]
  constructor() { }

  ngOnInit() {
    console.log(Date.now.toString())
  }

}
